<?php

function myErrorHandler($errno, $errstr, $errfile, $errline)
{
	$errtext = "<b>";
	switch($errno)
	{
		case (E_USER_ERROR):
			$errtext .= "Error";
			pass;
		case (E_USER_WARNING):
			$errtext .= "Warning";
			pass;
		case (E_USER_NOTICE):
			$errtext .= "Notice";
			pass;
		default:
			$errtext .= "Unknown (probably parse error)";
	}
	$errtext .= ":</b> ";
	$errtext .= $errstr . "<br/>";
	$errtext .= "In file <b>$errfile</b> line <b>$errline</b><br/>";
	$errtext .= "<b>User Agent:</b> " . $_SERVER["HTTP_USER_AGENT"];
	if(DISPLAY_ERRORS)
		errorWeb($errtext);
	
	die();
}

function errorWeb($errtext)
{
	echo $errtext;
}

set_error_handler("myErrorHandler");