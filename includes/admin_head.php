<?php

/**
 * This sets the admin package as well as checks for login and redirects if required
 */

$template_package = ADMIN_TEMPLATE_PACKAGE;

$require_login_default = true;

$require_login = (isset($require_login)) ? $require_login : $require_login_default;

if($require_login AND !(isset($_SESSION["admin.id"]) AND (string)$_SESSION["admin.id"] != ""))
{
	redirect(URL_ROOT . "admin/login/");
}