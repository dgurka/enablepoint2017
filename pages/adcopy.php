<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$id = (int)$matches[1];

$conn = Db::GetNewConnection();
$result = Db::ExecuteFirst("SELECT * FROM adcopies WHERE id = '$id'", $conn);
Db::CloseConnection($conn);

$renderpage = "<h2>".$result['title']."</h2>";
$renderpage .= $result['body'];

$context['title'] = $result['title'];
$context['body'] = $result['body'];

echo $twig->render('adcopy.html', $context);