<?php

require_once(BASE_DIR . "includes/cust_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

echo $twig->render("help_{$matches[1]}.html", $context);