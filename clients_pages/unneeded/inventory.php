<?php

require_once(BASE_DIR . "includes/cust_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

echo $twig->render('inventory.html', $context);