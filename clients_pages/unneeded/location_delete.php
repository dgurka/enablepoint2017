<?php

require_once(BASE_DIR . "includes/cust_head.php");
require_once(BASE_DIR . "bootstrap.php");

$conn = Db::GetNewConnection();

Db::ExecuteNonQuery("DELETE FROM directory WHERE ID = {$matches[1]}", $conn);

Db::CloseConnection($conn);

redirect(URL_ROOT . "clients/directory/");