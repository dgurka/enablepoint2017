<?php

require_once(BASE_DIR . "includes/cust_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

// Universal Pages

$custmenu = "<h3>Helpdesk Pages</h3>";

$menus = Db::ExecuteQuery("SELECT * FROM custpage WHERE menukey = 999 ORDER BY title ASC", $conn);

$custmenu .= "<ul>";

foreach ($menus as $value) 
{	
	$custmenu .= "<li><a href='../../clients/page/".$value["ID"]."/'>" . $value["title"] . "</a>"; 

}

$custmenu .= "</ul>";

// Private Client Pages

$custmenu .= "<h3>Client Pages</h3>";

$menus = Db::ExecuteQuery("SELECT * FROM custpage WHERE menukey = ".$_SESSION['cust.group']." ORDER BY title ASC", $conn);

$custmenu .= "<ul>";

foreach ($menus as $value) 
{	
	$custmenu .= "<li><a href='../../clients/page/".$value["ID"]."/'>" . $value["title"] . "</a>"; 

}

$custmenu .= "</ul>";

$context["pagemenu"] = $custmenu;

echo $twig->render('splash.html', $context);