<?php

require_once(BASE_DIR . "includes/cust_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$id = (int)$matches[1];

$conn = Db::GetNewConnection();
$page = Db::ExecuteFirst("SELECT * FROM custpage WHERE ID = '$id'", $conn);
Db::CloseConnection($conn);

if($page['menukey'] == $_SESSION['cust.group']){
	$disppage = (object)$page;
} else if($page['menukey'] == "999"){
	$disppage = (object)$page;
} else {
	$disppage = "You do not have permission to view this page";
}
//$context["page"] = (object)$page;
$context["page"] = $disppage;
echo $twig->render('page.html', $context);