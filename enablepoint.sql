-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 19, 2017 at 02:37 PM
-- Server version: 5.6.35-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `enablepoint`
--

-- --------------------------------------------------------

--
-- Table structure for table `adcopies`
--

CREATE TABLE IF NOT EXISTS `adcopies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caseno` int(11) NOT NULL,
  `title` text NOT NULL,
  `body` longtext NOT NULL,
  `part_name` text NOT NULL,
  `feeder_style` text NOT NULL,
  `OBSOLETE` varchar(5) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `key` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`key`, `value`) VALUES
('version_id', '3');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `notes` text NOT NULL,
  `locked` int(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`ID`, `username`, `password`, `email`, `notes`, `locked`) VALUES
(1, 'testaccount1', 'd41d8cd98f00b204e9800998ecf8427e', 'enablepoint@gmail.com\r\n', 'Test account for enablepoint. Password is password', 1),
(2, 'lockedaccount', '5f4dcc3b5aa765d61d8327deb882cf99', 'lockedaccount@enablepoint.com\r\n', 'password is password', 1),
(3, 'Frank', 'd41d8cd98f00b204e9800998ecf8427e', 'frankafarren@gmail.com', 'Shared 122', 1),
(4, 'DonTest', '1fa34d88f74fc8eef8c0b957b7f44eac', 'dgurka@enablepoint.com', '', 0),
(5, 'Livonia48154', 'd41d8cd98f00b204e9800998ecf8427e', 'joseph@enablepoint.com', 'can i change the email later?', 1),
(6, 'Support', '2bcd6f32a85b90d7dc4d0c176d539b10', 'support@alarmsupport.org', 'Alarm Support Old Shared', 0),
(7, 'FRANKAFARREN', 'd41d8cd98f00b204e9800998ecf8427e', 'frankAfarren@gmail.com', 'Frank A Farren Only', 0);

-- --------------------------------------------------------

--
-- Table structure for table `custpage`
--

CREATE TABLE IF NOT EXISTS `custpage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `content` text,
  `menukey` int(11) NOT NULL DEFAULT '-1',
  `menuorder` int(11) NOT NULL DEFAULT '0',
  `linkoverride` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT=' ' AUTO_INCREMENT=15 ;

--
-- Dumping data for table `custpage`
--

INSERT INTO `custpage` (`ID`, `title`, `description`, `content`, `menukey`, `menuorder`, `linkoverride`) VALUES
(1, 'EnablePoint Update Procedure', 'none', '<p style="text-align: left;">This is just a test page, don&#39;t get too excited.</p>\r\n', 1, 0, ''),
(2, 'Don Test Page', '', '<p>this is not assigned to a customer</p>\r\n', -1, 0, ''),
(3, 'Locked User Test Page', '', '<p>this is assigned to locked customer</p>\r\n', 2, 0, ''),
(4, 'LOGIN-MANUAL-INC', 'Video of how to Login, Look up an account, Add a manual incident', '<p style="text-align: left;">Introduction to Desktop FAB</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n', 1, 0, ''),
(5, 'DTF Waiver', 'How to Waive an alarm in Desktop FAB', '<p style="text-align: left;">How To Waive an alarm&nbsp;</p>\r\n\r\n<p style="text-align: left;"><a href="https://drive.google.com/open?id=0BzM0qrGsLahQMmVqN3NhMEIzOTQ">Click Here to Watch the Video</a></p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;"><a href="https://drive.google.com/open?id=0BzM0qrGsLahQMmVqN3NhMEIzOTQ">https://drive.google.com/open?id=0BzM0qrGsLahQMmVqN3NhMEIzOTQ</a></p>\r\n', -1, 0, ''),
(6, 'fab login webpage', 'tried to cut and paste including images', '<p><strong>FAB Login</strong></p>\r\n\r\n<p>The FAB login has three levels;</p>\r\n\r\n<p><strong>Administrator Menu</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Processor / User Menu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Read Only Menu</strong></p>\r\n\r\n<p><img height="550" src="file:///C:/Users/FAB/AppData/Local/Temp/msohtmlclip1/01/clip_image002.jpg" width="172" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img height="550" src="file:///C:/Users/FAB/AppData/Local/Temp/msohtmlclip1/01/clip_image004.jpg" width="175" />&nbsp;&nbsp;&nbsp;&nbsp; <img height="549" src="file:///C:/Users/FAB/AppData/Local/Temp/msohtmlclip1/01/clip_image006.jpg" width="174" /></p>\r\n\r\n<ol>\r\n	<li>The Administrator level has read/write access to every function except licensing.</li>\r\n	<li>The Processing level has read/write access to every function except administration of the system ( setup and configuration)</li>\r\n	<li>The user level has only the following functions and is READ ONLY:</li>\r\n</ol>\r\n\r\n<p><strong><u>Read only functionality allowed</u></strong></p>\r\n\r\n<p><strong>Billing functions</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Reprint Warning Letters</li>\r\n	<li>Invoice Summary Report</li>\r\n	<li>Account Invoice Detail</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Report functions </strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>All reports on the menu are available in read only mode.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Registration Reports</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Permit Renewal Report</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Accounts</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Account Info Lookup and Account History / Details</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Administration</strong></p>\r\n\r\n<ul>\r\n	<li>No functions available in administration (with one exception of MS Access database backup, for MS Access versions only*)\r\n	<ul>\r\n		<li>* MS Access versions will no longer be supported at the end of 2017</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<p align="center"><strong>Reference Screenshots for Read Only access to desktop FAB:</strong></p>\r\n\r\n<p><strong>Invoice Summary Report</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p align="center"><img height="362" src="file:///C:/Users/FAB/AppData/Local/Temp/msohtmlclip1/01/clip_image008.jpg" width="518" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Account Info Lookup and Account History / Details</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p align="center"><img height="338" src="file:///C:/Users/FAB/AppData/Local/Temp/msohtmlclip1/01/clip_image010.jpg" width="561" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p align="center"><img height="365" src="file:///C:/Users/FAB/AppData/Local/Temp/msohtmlclip1/01/clip_image012.jpg" width="557" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Right Click on the Grid above in account history to:</p>\r\n\r\n<p style="margin-left:1.5in;">i.Add a note</p>\r\n\r\n<p style="margin-left:1.5in;">ii.Add a false alarm incident manually</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 3, 0, ''),
(7, 'Desktop FAB Login function', 'only Frank can see this page ', '<h2 style="font-style:italic;">Desktop FAB</h2>\r\n\r\n<ul>\r\n	<li style="text-align: left;"><a href="https://drive.google.com/open?id=0B5e7at0pIPAVTGxFc19xSWN6Yzg">Login with Read Only Access</a></li>\r\n	<li style="text-align: left;">Login with Processor Access</li>\r\n	<li style="text-align: left;">Login with Administrative Access</li>\r\n</ul>\r\n\r\n<p><a href="https://drive.google.com/open?id=0B5e7at0pIPAVaVJJM0FmYjkteHM">Video Overview of Login and adding a manual alarm incident</a></p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<ul>\r\n	<li style="text-align: left;">Adding an alarm incident manually</li>\r\n	<li style="text-align: left;">Importing alarm incidents</li>\r\n	<li style="text-align: left;">&nbsp;</li>\r\n</ul>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n', 3, 0, ''),
(8, 'Sample Helpdesk Page', '', '<p>helpdesk page sample</p>\r\n\r\n<p>&nbsp;</p>\r\n', 999, 0, ''),
(9, 'Livonia Clemis Import', '', '<h2 style="font-style:italic;">City of Livonia - &nbsp;New CLEMIS CAD file</h2>\r\n\r\n<p style="text-align: left;">Livonia PD sent us a <a href="https://drive.google.com/open?id=0B5e7at0pIPAVMjZBeEtsZmdGZUU">sample false alarm file </a>for testing and here is our feedback for the file.</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;"><a href="https://drive.google.com/open?id=0B5e7at0pIPAVbGNlVlRhY014aDQ">Issue with Incident Number Cutting </a>off new CLEMIS file in Livonia database</p>\r\n\r\n<p style="text-align: left;"><a href="https://drive.google.com/open?id=0B5e7at0pIPAVd1VZSmozNnlYNlk">Sample Import video</a> with feedback from file given to us for trial week of 6/19</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;">d</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n', 5, 0, ''),
(10, 'Adding an Incident', 'Desktop FAB - Adding an Incident', '<p>Adding an Incident in Desktop FAB</p>\r\n\r\n<p><a href="https://drive.google.com/open?id=0B5e7at0pIPAVMmVOQ2YxbGIwMlU">Video</a></p>\r\n\r\n<p>Text:</p>\r\n\r\n<p>&nbsp;</p>\r\n', 6, 0, ''),
(11, 'Waiving an Incident', 'Waiving an incident in previous period and current period', '<p>Waivers</p>\r\n\r\n<p><a href="https://drive.google.com/open?id=0B5e7at0pIPAVWHVWczRjSF92SmM">Waive without printing waiver lettter</a></p>\r\n\r\n<p>Waive with printing waiver letter</p>\r\n\r\n<p>Waive in previous period without letter</p>\r\n\r\n<p>Waive in previous period with letter</p>\r\n\r\n<p>&nbsp;</p>\r\n', 6, 0, ''),
(12, 'Printing Warning Letters', 'Printing Warning Letters in WEBFAB', '<p>Printing Warning Letters - Add incidents and Print</p>\r\n\r\n<p>Printing Warning Letters - Second time ( reprint )&nbsp;</p>\r\n\r\n<p>Printing Warning Letters - During Close processing</p>\r\n\r\n<p>Reprinting Warning Letters from close processing</p>\r\n', 6, 0, ''),
(13, 'Create a new account', 'Creating a new account in Desktop FAB', '<p>&nbsp;</p>\r\n\r\n<p><a href="https://drive.google.com/file/d/0B5e7at0pIPAVaXhCZWlUUlVaTDA/view?usp=sharing">Creating a New Account</a> (after searching for it in database first)</p>\r\n', 6, 0, ''),
(14, 'live support sites', 'live support sites', '<p style="text-align: left;"><a href="https://liven-webfab-tay.alarmreg.com/Login.aspx?ReturnUrl=%2f">TAYLOR</a></p>\r\n\r\n<p style="text-align: left;"><a href="https://liven-webfab-lan.alarmreg.com/Login.aspx?ReturnUrl=%2f">LANSING</a></p>\r\n\r\n<p style="text-align: left;"><a href="https://liven-webfab-wat.alarmreg.com/Login.aspx?ReturnUrl=%2f">WATERFORD</a></p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n', 7, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `directory`
--

CREATE TABLE IF NOT EXISTS `directory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(45) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `hours` varchar(255) DEFAULT NULL,
  `img_loc` varchar(255) DEFAULT NULL,
  `description` text,
  `lat` varchar(45) DEFAULT NULL,
  `long` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `directory_cat`
--

CREATE TABLE IF NOT EXISTS `directory_cat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `directory_sub_cat`
--

CREATE TABLE IF NOT EXISTS `directory_sub_cat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `submitter_name` varchar(255) DEFAULT NULL,
  `submitter_phone` varchar(45) DEFAULT NULL,
  `submitter_email` varchar(255) DEFAULT NULL,
  `event_title` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `location_address` varchar(255) DEFAULT NULL,
  `description` text,
  `img_loc` varchar(255) DEFAULT NULL,
  `pending` tinyint(1) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image`
--

CREATE TABLE IF NOT EXISTS `gallery_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `gallery` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `imgloc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=113 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`ID`, `name`) VALUES
(1, 'Home'),
(2, 'About Us'),
(3, 'Products\r\n'),
(4, 'Technical Information'),
(5, 'Ad Copy Index'),
(6, 'Contact Us'),
(7, 'Request Quote');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `content` text,
  `menukey` int(11) NOT NULL DEFAULT '-1',
  `menuorder` int(11) NOT NULL DEFAULT '0',
  `linkoverride` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT=' ' AUTO_INCREMENT=23 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`ID`, `title`, `description`, `content`, `menukey`, `menuorder`, `linkoverride`) VALUES
(1, 'Home', '', '<!--\r\n    <!-- Header Carousel -->\r\n<header class="carousel slide" id="myCarousel"><!-- Indicators -->\r\n<header class="carousel slide" id="myCarousel"><!-- Wrapper for slides -->\r\n<div class="carousel-inner">\r\n<div class="item active">\r\n<div class="fill"><img src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\banner-tilted.png" style="width: 100%; height: auto; box-shadow: 5px 5px 5px #888888;" /></div>\r\n<br />\r\n<!--<h2>Caption 1</h2>\r\n                </div>\r\n            </div>\r\n            <!--<div class="item">\r\n                <div class="fill">\r\n				<img src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\police-office-workers3.png" style="height: auto; width: 100%;></div>\r\n                <div class="carousel-caption">\r\n                    <h2>Caption 2</h2>\r\n                </div>--></div>\r\n<!--\r\n            <div class="item">\r\n                <div class="fill"> \r\n					<img src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\911-center1.png" style="height: auto; width: 100%; min-width: inherit; min-height: 40%>\r\n				</div>\r\n                <div class="carousel-caption">\r\n				--><!--<h2>Caption 3</h2>--></div>\r\n<!-- Controls --></header>\r\n<!-- Controls --><!-- Page Content -->\r\n\r\n<div style="width: 85%; margin: auto; padding-top: .3%; text-align: center;"><a href="http://enablepoint.managedalarms.com/page/11/"><img alt="" src="http://enablepoint.managedalarms.com/inventory/fas_icon.png" style="width:20%; padding:15px;" /></a><a href="http://enablepoint.managedalarms.com/page/13/"><img alt="" src="http://enablepoint.managedalarms.com/inventory/oars_icon.png" style="width:20%; padding:15px;" /></a><a href="http://enablepoint.managedalarms.com/page/16/"><img alt="" src="http://enablepoint.managedalarms.com/inventory/csdb_icon.png" style="width:20%; padding:15px;" /></a><a href="#"><img alt="" src="http://enablepoint.managedalarms.com/inventory/gip_icon.png" style="width:20%; padding:15px;" /></a><a href="http://enablepoint.managedalarms.com/page/4/"><img alt="" src="http://enablepoint.managedalarms.com/inventory/amps_icon.png" style="width:20%; padding:15px;" /></a></div>\r\n\r\n<div style="width: 65%; margin: auto; padding-top: .3%; text-align: center;"><br />\r\n<a href="contact.html"><img src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\statepolicecar.jpg" style="width: 300px; height: auto; padding-bottom: 2%;" /> </a></div>\r\n</header>\r\n', -1, 0, ''),
(2, 'About Us', 'About Us', '<!-- Page Content -->\r\n<div class="container"><!-- Page Heading/Breadcrumbs -->\r\n<div class="row">\r\n<div class="col-lg-12">&nbsp;\r\n<h1 class="page-header">About Us</h1>\r\n</div>\r\n</div>\r\n<!-- /.row --><!-- Intro Content -->\r\n\r\n<div class="row">\r\n<div class="col-md-6"><img alt="" class="img-responsive" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\EnablePointLogo.png" style="margin-top:2%;" /> <img alt="" class="img-responsive" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\Building-Touch-Up-Tall-Smalls.png" style="margin-top:5%;" /></div>\r\n\r\n<div class="col-md-6">&nbsp;</div>\r\n\r\n<div class="col-md-6"><span style="font-size:18px;">EnablePoint is a full service software development and services company serving law enforcement and municipalities nationwide.&nbsp;</span></div>\r\n\r\n<div class="col-md-6">&nbsp;</div>\r\n\r\n<div class="col-md-6"><span style="font-size:18px;">We provide Software and web development for municipalities, private business, and Law Enforcement. EnablePoint specializes in alarm registration and false alarm tracking software for municipal clients.</span></div>\r\n\r\n<div class="col-md-6">&nbsp;</div>\r\n\r\n<div class="col-md-6"><span style="font-size:18px;">Online products include Online Alarm Registration System (OARS), Web Based False Alarm Billing (WEBFAB), and &nbsp;recently a Weapons, Radio, Uniform, Training, Certifications and Performance Inventory Program for first responders (IP). EnablePoint continues to support and develop our desktop False Alarm Billing client server program, FAB.</span></div>\r\n\r\n<div class="col-md-6">&nbsp;</div>\r\n\r\n<div class="col-md-6"><span style="font-size:18px;">EnablePoint&#39;s software products are used by our own staff, where we process false alarm incidents for our clients. &nbsp;Police and/or Finance sends us the false alarm incident file, we process all false alarms, registrations, payments, and waivers approved and the alarm payments go direct to the client.&nbsp;</span></div>\r\n\r\n<div class="col-md-6">&nbsp;</div>\r\n\r\n<div class="col-md-6">&nbsp;</div>\r\n\r\n<div class="col-md-6"><span style="font-size:18px;">Contact us by calling 734.268.6058</span></div>\r\n\r\n<div class="col-md-6">&nbsp;</div>\r\n\r\n<div class="col-md-6">&nbsp;</div>\r\n\r\n<div class="col-md-6"><img alt="False Alarm Information Tracking Software " src="http://enablepoint.managedalarms.com/inventory/fas_icon.png" style="width: 338px; height: 169px;" /></div>\r\n\r\n<div class="col-md-6">\r\n<h4 style="font-family: Times New Roman; font-size: 20px;">&nbsp;</h4>\r\n</div>\r\n</div>\r\n\r\n<div class="row">\r\n<div class="col-md-6">\r\n<h4 style="font-family: Times New Roman; font-size: 20px;">&nbsp;</h4>\r\n</div>\r\n</div>\r\n<!-- /.row --></div>\r\n', 0, 2, ''),
(3, 'Products', 'Products', '<!-- Page Content -->\r\n<div class="container"><!-- Page Heading/Breadcrumbs -->\r\n<div class="row">\r\n<div class="col-lg-12">&nbsp;\r\n<h1 class="page-header">Municipal and Law Enforcement Products&nbsp;</h1>\r\n</div>\r\n</div>\r\n<!-- /.row --><!-- Image Header -->\r\n\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets/images/services-police-lights.png" style="width: 70%; height: auto; " /></div>\r\n</div>\r\n<!-- /.row --><!-- Service Panels --><!-- The circle icons use Font Awesome''s stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">EnablePoint provides a comprehensive suite of &nbsp;alarm management software&nbsp;and services&nbsp;for cities, counties, and villages. Our approach is to provide a customized and exact match to the needs of our clients, by providing only the software and/or services needed or requested, and deliver a tailored solution using our experience and alarm management knowledge gained over the last 16 years. &nbsp;Customization specific to a client needs during implementation is desired over the client making a sacrifice to their preferred processes and policies.&nbsp;</h4>\r\n\r\n<div style="width: 85%; margin: auto; padding-top: .3%; text-align: center;"><a href="http://enablepoint.managedalarms.com/page/11/"><img alt="" src="http://enablepoint.managedalarms.com/inventory/fas_icon.png" style="width:20%; padding:15px;" /></a><a href="http://enablepoint.managedalarms.com/page/13/"><img alt="" src="http://enablepoint.managedalarms.com/inventory/oars_icon.png" style="width:20%; padding:15px;" /></a><a href="http://enablepoint.managedalarms.com/page/16/"><img alt="" src="http://enablepoint.managedalarms.com/inventory/csdb_icon.png" style="width:20%; padding:15px;" /></a><a href="#"><img alt="" src="http://enablepoint.managedalarms.com/inventory/gip_icon.png" style="width:20%; padding:15px;" /></a><a href="http://enablepoint.managedalarms.com/page/4/"><img alt="" src="http://enablepoint.managedalarms.com/inventory/amps_icon.png" style="width:20%; padding:15px;" /></a></div>\r\n</div>\r\n</div>\r\n', 0, 3, ''),
(4, 'AMPS1', 'Processing Services\r\n', '<!-- Page Content -->\r\n<div class="container"><!-- Page Heading/Breadcrumbs -->\r\n<div class="row">\r\n<div class="col-lg-12">&nbsp;\r\n<h1 class="page-header">Alarm Management Processing Services (AMPS)</h1>\r\n</div>\r\n</div>\r\n<!-- /.row --><!--<!-- Image Header -->\r\n\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets/images/office-paperwork-smaller.png" style="width: 60%; height: auto; " /></div>\r\n</div>\r\n<!-- /.row --><!-- Service Panels --><!-- The circle icons use Font Awesome''s stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">The approach EnablePoint Inc. takes to alarm management processing services is&nbsp;unique and very simple; you are in full control; <strong>We do the work for you.</strong></h4>\r\n\r\n<div class="container"><!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n<div class="row">\r\n<p style="text-align: left;">Unlike other processing services available today, we are not aggressive and always position ourselves as your jurisdictions customer service working for the best interest of your citizens and the effort to reduce false alarms.&nbsp; Our focus is not on maximizing billing.&nbsp;&nbsp; Our focus is on running an efficient false alarm management system and helping your alarm owners reduce false alarms which results in savings to your jurisdiction.</p>\r\n\r\n<p style="text-align: left;">The process is simplified into a few easy steps;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="margin-left: 0.5in; text-align: left;"><strong>Your jurisdiction sends us the false alarm incidents </strong></p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">We import incidents into our web-based False Alarm Billing (FAB) System, the system calculates which alarm address is to receive a warning, which an invoice, and the appropriate documents are printed in batch, processed, and mailed out by our staff.</p>\r\n\r\n<p style="margin-left: 0.5in; text-align: left;">&nbsp;</p>\r\n\r\n<p style="margin-left: 1.5in; text-align: left;">i.Warning Letters</p>\r\n\r\n<p style="margin-left: 1.5in; text-align: left;">ii.Invoices</p>\r\n\r\n<p style="margin-left: 1.5in; text-align: left;">iii.Request to register unregistered sites identified from false alarm incident.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">&nbsp;</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">â—‹We receive the initial phone call and talk to your citizens;&nbsp; should they choose to call for more information we will do our best to resolve questions and other issues, such as billing address changes as professionals.&nbsp; We will not ask unrelated questions or pressure your citizens in any way.&nbsp; We will act on behalf of the alarm owner.&nbsp; For issues that require a management decision, we document any escalation requests or validation of incident occurrence from your designated alarm management contact and present them with factual information and complete data.&nbsp; The decision to waive, bill, deny waiver, etc. will always fall into your jurisdiction&rsquo;s responsibility;&nbsp; We do the research and detail work for you, you make the decisions.</p>\r\n\r\n<p style="margin-left: 0.5in; text-align: left;">&nbsp;</p>\r\n\r\n<p style="margin-left: 0.5in; text-align: left;"><strong>You Receive the Payment(s) for alarm registration or false alarms directly.</strong></p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;"><strong>We trust you.</strong> Provide us with a report or copies of the payments that have been received and we will key in the payments to the web based&nbsp; False Alarm Billing (FAB) system.&nbsp; You will have online access to review the reports and match them up with your receipts.&nbsp; We handle the payments this way for three reasons;&nbsp; it is much simpler,&nbsp; it makes sense, and it keeps our insurances and costs low, a benefit for both EnablePoint and your jurisdiction in the form of pricing.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;"><strong>We process your online alarm registrations directly to FAB and we also key in your paper registrations.&nbsp; </strong>Our Online Alarm Registration System (OARS) is set up to match your paper registration that you have been using as close as technically possible.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">Your alarm owners create an account using a validated email address and submit all the registration information that you need.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">If online payment is desired, we have over seven different interfaces credit card merchant companies and will set additional companies up if not on our list already.&nbsp;&nbsp; As with false alarm payments,&nbsp; registration payments will go directly into your account and we will not touch your funds.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">We will have a support line setup during business hours to help your citizens register online.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">For those that do not wish to register online, we will key in the paper registration data and payments in for you.&nbsp; <strong>We do the work; you make the decisions.</strong></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 0, 4, ''),
(5, 'Software Solutions (NOT USED)', 'Software Solutions', '<!-- Page Content -->\r\n<div class="container"><!-- Page Heading/Breadcrumbs -->\r\n<div class="row">\r\n<div class="col-lg-12">&nbsp;\r\n<h1 class="page-header">Software Solutions</h1>\r\n</div>\r\n</div>\r\n<!-- /.row --><!--<!-- Image Header -->\r\n\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>\r\n<!-- /.row --><!-- Service Panels --><!-- The circle icons use Font Awesome''s stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n&nbsp;\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="panel panel-default text-center">\r\n<div class="panel-heading" style="background-color: #fffff;"><span class="fa-stack fa-5x"><img class="fab-image-circle" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\csdb-red-text.png" style="width: 110%; height: auto;" /> </span></div>\r\n\r\n<div class="panel-body">\r\n<h3><strong>Community Service Database Billing </strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4 style="text-align: center; font-family:Times New Roman;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n<br />\r\n<a class="btn btn-primary" href="fab.html">Learn More</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="panel panel-default text-center">\r\n<div class="panel-heading" style="background-color: #ffff;"><span class="fa-stack fa-5x"><img class="oars-image-circle" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\mst-logo.png" style="width: 100%; height: auto;" /> </span></div>\r\n\r\n<div class="panel-body">\r\n<h3><strong>MST :</strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4 style="text-align: center; font-family:Times New Roman;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n<br />\r\n<a class="btn btn-primary" href="oars.html">Learn More</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="panel panel-default text-center">\r\n<div class="panel-heading" style="background-color: #fffff"><span class="fa-stack fa-5x"><img class="oars-img-circle" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\livonia-save-our-youth.png" style="width: 110%; height: auto;" /> </span></div>\r\n\r\n<div class="panel-body">\r\n<h3><strong>Livonia Save Our Youth Coallition :</strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4 style="text-align: center; font-family:Times New Roman;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n<br />\r\n<a class="btn btn-primary" href="http://www.livoniasaveouryouth.org/">Go To Site</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="panel panel-default text-center">\r\n<div class="panel-heading" style="background-color: #ffff;"><span class="fa-stack fa-5x"><img class="fab-image-circle" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\nehaisil-park.png" style="width: 100%; height: auto;" /> </span></div>\r\n\r\n<div class="panel-body">\r\n<h3><strong>Nehaisil Park :</strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4 style="text-align: center; font-family:Times New Roman;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n<br />\r\n<a class="btn btn-primary" href="http://nehasilpark.org/">Go To Site</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="panel panel-default text-center">\r\n<div class="panel-heading" style="background-color: #fffff;"><span class="fa-stack fa-5x"><img class="fab-image-circle" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\spectrum-logo.png" style="width: 110%; height: auto;" /> </span></div>\r\n\r\n<div class="panel-body">\r\n<h3><strong>Spectrum Automation : </strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4 style="text-align: center; font-family:Times New Roman;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n<br />\r\n<a class="btn btn-primary" href="http://www.spectrumautomation.com/">Go To Site</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="panel panel-default text-center">\r\n<div class="panel-heading" style="background-color: #ffff;"><span class="fa-stack fa-5x"><img class="oars-image-circle" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\dalys-logo.png" style="width: 100%; height: auto;" /> </span></div>\r\n\r\n<div class="panel-body">\r\n<h3><strong>Daly`s Restaurant :</strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4 style="text-align: center; font-family:Times New Roman;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n<br />\r\n<a class="btn btn-primary" href="http://www.dalyrestaurants.com/">Go To Site</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="panel panel-default text-center">\r\n<div class="panel-heading" style="background-color: #fffff"><span class="fa-stack fa-5x"><img class="oars-img-circle" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\livonia-chamber-logo.png" style="width: 110%; height: auto;" /> </span></div>\r\n\r\n<div class="panel-body">\r\n<h3><strong>Header :</strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4 style="text-align: center; font-family:Times New Roman;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n<br />\r\n<a class="btn btn-primary" href="http://livonia.org/">Go To Site</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class="col-md-3 col-sm-6">\r\n<div class="panel panel-default text-center">\r\n<div class="panel-heading" style="background-color: #ffff;"><span class="fa-stack fa-5x"><img class="fab-image-circle" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\nehaisil-park.png" style="width: 100%; height: auto;" /> </span></div>\r\n\r\n<div class="panel-body">\r\n<h3><strong>Header :</strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4 style="text-align: center; font-family:Times New Roman;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n<br />\r\n<a class="btn btn-primary" href="#">Learn More</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n', -1, 5, ''),
(6, 'Importing False Alarms from CAD', '', '<!-- Page Content -->\r\n<div class="container"><!-- Page Heading/Breadcrumbs -->\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header" style="text-align: left;">Importing False Alarms from CAD</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph" style="text-align: left;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n</div>\r\n</div>\r\n', -1, 6, ''),
(7, 'Exporting False Alarm and Registration Charges to Treasury Software', '', '<!-- Page Content -->\r\n<div class="container">\r\n<!-- Page Heading/Breadcrumbs -->\r\n\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header">Exporting False Alarm and Registration Charges to Treasury Software</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n</div>\r\n</div>\r\n', -1, 7, ''),
(8, 'Interface Development for Online Payments, Parcel Number', '', '<!-- Page Content -->\r\n<div class="container">\r\n<!-- Page Heading/Breadcrumbs -->\r\n\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header">Interface Development for Online Payments, Parcel Number</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n</div>\r\n</div>\r\n', -1, 5, ''),
(9, 'Website Development and Charity Work', '', '<!-- Page Content -->\r\n<div class="container">\r\n<!-- Page Heading/Breadcrumbs -->\r\n\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header">Website Development and Charity Work</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n</div>\r\n</div>\r\n', -1, 6, ''),
(10, 'old AMPS page 10', '', '<!-- Page Content -->\r\n<div class="container"><!-- Page Heading/Breadcrumbs -->\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header">Alarm Management Services</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">what page is this page 10 again ??</h4>\r\n</div>\r\n</div>\r\n', -1, 7, ''),
(11, 'False Alarm Billing', '', '<!-- Page Content -->\r\n<div class="container"><!-- Page Heading/Breadcrumbs -->\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header">False Alarm Billing (FAB)</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph" style="text-align: left;"><strong eo-user-id="662728142539960320" eo-user-org-id="662728142544154625" eo-user-room="room_9307ec4a01d3f1b370ba8ca48542fae8">Alejandra Valencia</strong>&nbsp;<a href="https://www.upwork.com/" ng-click="goToStory($event, attach)" title="Monday, June 19, 2017 6:45 AM">Monday, June 19, 2017 6:45 AM</a></h4>\r\n\r\n<p>Hello Frank, Yes, it helps a lot!, about the site: something I see that we can correct is to give a little more of importance to the home showing a little more content about the products and services, currently the site in the home is only made with images and is not very good for SEO , It is also very difficult to navigate and read the content on a mobile device, another thing we can do in the internal pages is to improve the way in which the content is organized a bit, for example in products we can use images or icons more striking and Organize the columns to make reading more comfortable, in general we can play a little more with the corporate colors, keeping the style sober and clean original, but improving the buttons and items you are using default bootstrap and improve the responsive a little.<br />\r\n<br />\r\nIf you want we can make a wireframe in which you can make revisions and corrections for have clearer the general design, or start working on the site from the beginning, as this be better for you</p>\r\n\r\n<p>&nbsp;</p>\r\n</div>\r\n</div>\r\n', -1, 8, ''),
(13, 'OARS', '', '<!-- Page Content -->\r\n<div class="container"><!-- Page Heading/Breadcrumbs -->\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header">Online Alarm Registration&nbsp;System&nbsp;(OARS)</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">Your online alarm registration system will be setup to mimic your existing paper registration form as closely as possible. &nbsp;Each one of our clients receive a customized registration layout screen. &nbsp;</h4>\r\n</div>\r\n</div>\r\n', -1, 0, ''),
(14, 'WEBFAB', '', '<!-- Page Content -->\r\n<div class="container">\r\n<!-- Page Heading/Breadcrumbs -->\r\n\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header">WEBFAB</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n</div>\r\n</div>\r\n', -1, 3, ''),
(15, 'AMPS2', '', '<!-- Page Content -->\r\n<div class="container"><!-- Page Heading/Breadcrumbs -->\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header">Alarm Management Processing Service (AMPS)</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<p style="text-align: left;">The approach EnablePoint Inc. takes to alarm management processing services is someone unique and very simple; you are in full control of your citizens and alarm owners, and you make the billing decisions; We do the work for you.</p>\r\n\r\n<p style="text-align: left;">2</p>\r\n\r\n<p style="text-align: left;">Unlike other processing services available today, we are not aggressive and always position ourselves as your jurisdictions customer service working for the best interest of your citizens and the effort to reduce false alarms.&nbsp; Our focus is not on maximizing billing.&nbsp;&nbsp; Our focus is on running an efficient false alarm management system and helping your alarm owners reduce false alarms which results in savings to your jurisdiction.</p>\r\n\r\n<p style="text-align: left;">The process is simplified into a few easy steps;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="margin-left: 0.5in; text-align: left;"><strong>Your jurisdiction sends us the false alarm incidents </strong></p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">We import incidents into our web-based False Alarm Billing (FAB) System, the system calculates which alarm address is to receive a warning, which an invoice, and the appropriate documents are printed in batch, processed, and mailed out by our staff.</p>\r\n\r\n<p style="margin-left: 0.5in; text-align: left;">&nbsp;</p>\r\n\r\n<p style="margin-left: 1.5in; text-align: left;">i.Warning Letters</p>\r\n\r\n<p style="margin-left: 1.5in; text-align: left;">ii.Invoices</p>\r\n\r\n<p style="margin-left: 1.5in; text-align: left;">iii.Request to register unregistered sites identified from false alarm incident.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">&nbsp;</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">â—‹We receive the initial phone call and talk to your citizens;&nbsp; should they choose to call for more information we will do our best to resolve questions and other issues, such as billing address changes as professionals.&nbsp; We will not ask unrelated questions or pressure your citizens in any way.&nbsp; We will act on behalf of the alarm owner.&nbsp; For issues that require a management decision, we document any escalation requests or validation of incident occurrence from your designated alarm management contact and present them with factual information and complete data.&nbsp; The decision to waive, bill, deny waiver, etc. will always fall into your jurisdiction&rsquo;s responsibility;&nbsp; We do the research and detail work for you, you make the decisions.</p>\r\n\r\n<p style="margin-left: 0.5in; text-align: left;">&nbsp;</p>\r\n\r\n<p style="margin-left: 0.5in; text-align: left;"><strong>You Receive the Payment(s) for alarm registration or false alarms directly.</strong></p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;"><strong>We trust you.</strong> Provide us with a report or copies of the payments that have been received and we will key in the payments to the web based&nbsp; False Alarm Billing (FAB) system.&nbsp; You will have online access to review the reports and match them up with your receipts.&nbsp; We handle the payments this way for three reasons;&nbsp; it is much simpler,&nbsp; it makes sense, and it keeps our insurances and costs low, a benefit for both EnablePoint and your jurisdiction in the form of pricing.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;"><strong>We process your online alarm registrations directly to FAB and we also key in your paper registrations.&nbsp; </strong>Our Online Alarm Registration System (OARS) is set up to match your paper registration that you have been using as close as technically possible.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">Your alarm owners create an account using a validated email address and submit all the registration information that you need.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">If online payment is desired, we have over seven different interfaces credit card merchant companies and will set additional companies up if not on our list already.&nbsp;&nbsp; As with false alarm payments,&nbsp; registration payments will go directly into your account and we will not touch your funds.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">We will have a support line setup during business hours to help your citizens register online.</p>\r\n\r\n<p style="margin-left: 1in; text-align: left;">For those that do not wish to register online, we will key in the paper registration data and payments in for you.&nbsp; <strong>We do the work; you make the decisions.</strong></p>\r\n</div>\r\n</div>\r\n', -1, 0, ''),
(16, 'CSDB', '', '<!-- Page Content -->\r\n<div class="container">\r\n<!-- Page Heading/Breadcrumbs -->\r\n\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header">CSDB</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n</div>\r\n</div>\r\n', -1, 0, ''),
(17, 'MST', '', '<!-- Page Content -->\r\n<div class="container">\r\n<!-- Page Heading/Breadcrumbs -->\r\n\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header">Membership Tracking (MST)</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n</div>\r\n</div>\r\n', -1, 0, ''),
(18, 'Websites by the Menu (WSBTM)', '', '<!-- Page Content -->\r\n<div class="container"><!-- Page Heading/Breadcrumbs -->\r\n<div class="row">\r\n<div class="col-lg-12">\r\n<h1 class="page-header">Websites by the Menu (WSBTM)</h1>\r\n</div>\r\n</div>\r\n<!--  Header --><!--\r\n<div class="row">\r\n<div class="col-lg-12"><img alt="" class="img-responsive1" src="..\\..\\..\\..\\templates\\enablepoint\\assets\\images\\programmer-small.png" style="width: 70%; height: auto; " /></div>\r\n</div>-->\r\n\r\n<div class="row">\r\n<h4 class="services-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>\r\n</div>\r\n</div>\r\n', -1, 0, ''),
(19, 'Location', 'Location of EnablePoint Office', '<h3><img alt="" src="http://enablepoint.managedalarms.com/inventory/images_by_faf/close-up-enablepoint.png" style="margin-right: 10px; float: left; width: 100px; height: 100px;" />Location</h3>\r\n\r\n<h2 style="text-align: left;">Location</h2>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;"><span style="font-size:20px;">EnablePoint Inc. is located in the Plymouth Square shopping center offices area located next to the Mobil gas station on the southeast corner of Plymouth and Merriman. &nbsp; The shopping center faces Plymouth Road, and the offices face Denne street, on the east side of the building. &nbsp;You can enter in the main entrance, or park in the rear of the building, and enter in the rear door, which is easier. &nbsp;go upstairs to suite 208.</span></p>\r\n\r\n<p><img alt="" src="http://enablepoint.managedalarms.com/inventory/images_by_faf/close-up-enablepoint.png" style="width: 545px; height: 501px;" /></p>\r\n\r\n<p><img alt="" src="http://enablepoint.managedalarms.com/inventory/images_by_faf/wide-area.png" style="width: 885px; height: 562px;" /></p>\r\n', -1, 0, ''),
(20, 'Job Postings and Employment', 'Job Posting for enablepoint\r\n\r\n', '<h2 style="text-align: left;">Intermediate level C# Programming and Support position opening soon<small>&nbsp;(Livonia)</small></h2>\r\n\r\n<section>\r\n<p style="text-align: left;">compensation: negotiable&nbsp;<br />\r\nemployment type:&nbsp;full-time or part time&nbsp;</p>\r\n\r\n<section id="postingbody">\r\n<p style="text-align: left;">EnablePoint Inc., a&nbsp;local small, established software company is seeking both part time and&nbsp;full time Visual Studio C# .NET Developers AND Support people interested in programming and software&nbsp;to join our team! The positions will require you to&nbsp;develop and implement unique web-based software applications, work with our end users, and participate in / stay in the loop from new idea, concept, scope, development, testing, implementation and support. We are looking for a &#39;one person does it all&#39; candidate.<br />\r\n<br />\r\n<strong>Responsibilities:</strong></p>\r\n\r\n<ul>\r\n	<li style="text-align: left;">Work with end users (sometimes very technically illiterate but very nice people) and see things from their point of view <strong>(important!)</strong> mostly remotely, and sometimes on site.</li>\r\n	<li style="text-align: left;">Work with results oriented small business owners and follow procedures and policies in place.</li>\r\n	<li style="text-align: left;">Assist in creating new processes and policies as we continue to grow the business.</li>\r\n	<li style="text-align: left;">Design, create, and modify solutions for the software applications WEBFAB, OARS, Inventory Program, and other products&nbsp;and features.</li>\r\n	<li style="text-align: left;">Convert written, graphic, audio, and video components to compatible web formats</li>\r\n	<li style="text-align: left;">Create back-end code and interfaces for existing product.</li>\r\n	<li style="text-align: left;">Analyze user needs to implement content, performance, and capacity</li>\r\n	<li style="text-align: left;">Integrate system with other computer applications</li>\r\n	<li style="text-align: left;">Keep up-to-date on web developments and trends</li>\r\n</ul>\r\n\r\n<p style="text-align: left;"><br />\r\n<strong>Qualifications:</strong></p>\r\n\r\n<ul>\r\n	<li style="text-align: left;">Previous experience in C# web development or other related fields</li>\r\n	<li style="text-align: left;">Previous experience in MS SQL databases, stored procedures, and IIS</li>\r\n	<li style="text-align: left;">Previouse experience with software repositories and backup systems</li>\r\n	<li style="text-align: left;">Familiarity with with HTML, Javascript, CSS, and other related languages</li>\r\n	<li style="text-align: left;">Strong problem solving and critical thinking skills</li>\r\n	<li style="text-align: left;">Strong attention to detail</li>\r\n</ul>\r\n\r\n<p style="text-align: left;"><br />\r\nThis position has existed in the form of two or three part time employees and contractors, and we are ready to add more full or part time, dedicated and loyal programmers that can work well in a small business and demanding environment. We are looking for someone to work in the office, however would consider a part time remote employee that demonstrates they are a perfect fit, including a proven support background. We consider human interaction very important, therefore the successfully hired candidate will live in the Detroit area or Southwest Florida, our owners main home work bases.<br />\r\n<br />\r\nPlease apply ONLY with email and Text response in your email - Please do not attach anything, we would like to see a resume and introduction in the body of the email for faster review of candidates. Multiple email follow ups are fine, if you would like feedback as to why we are not calling you we will be happy to answer. Single submissions will not be answered unless we are interested in talking. This will be a long term interview process as we want candidates that share&nbsp;our values, desires and goals to grow our company, someone who wants to work in a fast paced environment like ours, and not one that simply needs a job. We are NOT interested in a search firm or contractor.<br />\r\n<br />\r\nPlease reply with &#39;long term 2017&#39; in the subject line of your email to admin at enablepoint (dot) com</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n</section>\r\n</section>\r\n', -1, 0, ''),
(21, 'Client Login', '', '<p style="text-align: left;">1. &nbsp;side note need hlep with margins</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;">2. now here is why wer are here</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;">3. &nbsp; &nbsp;client login</p>\r\n\r\n<ul>\r\n	<li style="text-align: left;">each customer had their own login user and pwd</li>\r\n	<li style="text-align: left;">we can see by email or report when and if they login and what pages they look at&nbsp;</li>\r\n	<li style="text-align: left;">we get a notice when they login</li>\r\n	<li style="text-align: left;">I have the abilty to add pages just like the public site prefer to base it on cms</li>\r\n	<li style="text-align: left;">i will create the pages per customer - most pages will be for ALL logins but some pages might be for SOME logins</li>\r\n	<li style="text-align: left;">select public ( all customers) &nbsp;or select certain customers to see the page &nbsp;- also option for non logged in users(minor)</li>\r\n</ul>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;">in other words, if i have a support issue that is complex, i want to share with customer directions , but do not want public to see. &nbsp;</p>\r\n\r\n<p style="text-align: left;">instead of emailing back and forth.&nbsp;</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n', -1, 0, ''),
(22, 'links ', '', '<p style="text-align: left;"><a href="https://liven-webfab-tay.alarmreg.com/Login.aspx?ReturnUrl=%2f">TAYLOR</a></p>\r\n\r\n<p style="text-align: left;"><a href="https://liven-webfab-lan.alarmreg.com/Login.aspx?ReturnUrl=%2f">LANSING</a></p>\r\n\r\n<p style="text-align: left;"><a href="https://liven-webfab-wat.alarmreg.com/Login.aspx?ReturnUrl=%2f">WATERFORD</a></p>\r\n\r\n<p style="text-align: left;">&nbsp;</p>\r\n', 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `username`, `password`) VALUES
(1, 'enablepointadmin', 'ypsilanti999');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `content` text,
  `menukey` int(11) NOT NULL DEFAULT '-1',
  `menuorder` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL DEFAULT '',
  `keywords` text NOT NULL,
  `part_name` text NOT NULL,
  `feeder_style` text NOT NULL,
  `featured` int(1) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' ' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
