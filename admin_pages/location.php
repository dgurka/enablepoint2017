<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	// $category = Db::EscapeString(post("category"), $conn); // not needed
	$subcategory = Db::EscapeString(post("subcategory"), $conn);
	$name = Db::EscapeString(post("name"), $conn);
	$address = Db::EscapeString(post("address"), $conn);
	$address_1 = Db::EscapeString(post("address_1"), $conn);
	$city = Db::EscapeString(post("city"), $conn);
	$state = Db::EscapeString(post("state"), $conn);
	$zip = Db::EscapeString(post("zip"), $conn);
	$phone = Db::EscapeString(post("phone"), $conn);
	$fax = Db::EscapeString(post("fax"), $conn);
	$email = Db::EscapeString(post("email"), $conn);
	$contact_person = Db::EscapeString(post("contact_person"), $conn);
	$contact_phone = Db::EscapeString(post("contact_phone"), $conn);
	$website = Db::EscapeString(post("website"), $conn);
	$hours = Db::EscapeString(post("hours"), $conn);
	$description = Db::EscapeString(post("description"), $conn);
	$lat = Db::EscapeString(post("lat"), $conn);
	$long = Db::EscapeString(post("long"), $conn);

	if($lat == "")
	{
		// get the geocoding
		$coding = GeoCoder::getGeoCoding(
			$address . " " . $city . " " . $state . " " . $zip
		);
		if($coding != null)
		{
			$lat = $coding->lat;
			$long = $coding->lng;
		}
	}


	$img_loc = saveFile("image", "directory");

	if($img_loc !== false)
		$img_loc = " , img_loc = '$img_loc'";
	else
		$img_loc = "";

	if(isset($matches[1]))
	{
		$id = (int)$matches[1];

		$query = "UPDATE directory SET `parent` = '$subcategory', `name` = '$name', `address` = '$address', `address_2` = '$address_1', `city` = '$city', `state` = '$state', `zip` = '$zip', `phone` = '$phone', `fax` = '$fax', `email` = '$email', `contact_person` = '$contact_person', `contact_phone` = '$contact_phone', `website` = '$website', `hours` = '$hours', `description` = '$description', `lat` = '$lat', `long` = '$long' $img_loc WHERE ID = $id";

		Db::ExecuteNonQuery($query, $conn);
		redirect(URL_ROOT . "admin/location/" . $id . "/");
	}
	else
	{
		$query = "INSERT INTO directory SET `parent` = '$subcategory', `name` = '$name', `address` = '$address', `address_2` = '$address_1', `city` = '$city', `state` = '$state', `zip` = '$zip', `phone` = '$phone', `fax` = '$fax', `email` = '$email', `contact_person` = '$contact_person', `contact_phone` = '$contact_phone', `website` = '$website', `hours` = '$hours', `description` = '$description', `lat` = '$lat', `long` = '$long' $img_loc";

		Db::ExecuteNonQuery($query, $conn);
		$id = Db::GetLastInsertID($conn);
		redirect(URL_ROOT . "admin/location/" . $id . "/");
	}

	exit();
}

if(isset($matches[1]))
{
	$id = (int)$matches[1];
	$location = Db::ExecuteFirst("SELECT directory_sub_cat.parent as category, directory.* FROM directory INNER JOIN directory_sub_cat ON directory.parent = directory_sub_cat.ID WHERE directory.ID = $id", $conn);
	$category = $location["category"];
	$subcategory = $location["parent"];
	$name = q($location["name"]);
	$address = q($location["address"]);
	$address_1 = q($location["address_2"]);
	$city = q($location["city"]);
	$state = q($location["state"]);
	$zip = q($location["zip"]);
	$phone = q($location["phone"]);
	$fax = q($location["fax"]);
	$email = q($location["email"]);
	$contact_person = q($location["contact_person"]);
	$contact_phone = q($location["contact_phone"]);
	$website = q($location["website"]);
	$hours = q($location["hours"]);
	$img_loc = $location["img_loc"];
	$description = $location["description"];
	$lat = q($location["lat"]);
	$long = q($location["long"]);
}
else
{
	$category = (int)get("cat", -1);
	$subcategory = (int)get("subcat", -1);
	$name = "";
	$address = "";
	$address_1 = "";
	$city = "";
	$state = "";
	$zip = "";
	$phone = "";
	$fax = "";
	$email = "";
	$contact_person = "";
	$contact_phone = "";
	$website = "";
	$hours = "";
	$img_loc = "";
	$description = "";
	$lat = "";
	$long = "";
}

$_cats = Db::ExecuteQuery("SELECT * FROM directory_cat", $conn);
$_sub_cats = Db::ExecuteQuery("SELECT * FROM directory_sub_cat", $conn);

$_cats_arr = array("-1" => "Please Choose");

foreach ($_cats as $i => $value) 
{
	$_cats_arr[(string)$value["ID"]] = $value["name"];
}

$json_buff = "";
foreach ($_cats as $value) 
{
	$json_buff .= "subCats[\"{$value["ID"]}\"] = [\n";
	foreach ($_sub_cats as $sub_cat) 
	{
		if($sub_cat["parent"] == $value["ID"])
		{
			$json_buff .= "\t[\"{$sub_cat["ID"]}\", \"{$sub_cat["name"]}\"],\n";
		}
	}
	$json_buff .= "];\n\n";
}

$cats = makeOptions($_cats_arr, $category);

$subcatslist = "";
$subcatsarr = array();

if($category >= 0)
{
	// fill out the initial list
	foreach ($_sub_cats as $i => $sub_cat) 
	{
		if($sub_cat["parent"] == $category)
		{
			$subcatsarr[$sub_cat["ID"]] = $sub_cat["name"];
		}
	}
}

$context["name"] = $name;
$context["address"] = $address;
$context["address_1"] = $address_1;
$context["city"] = $city;
$context["state"] = $state;
$context["zip"] = $zip;
$context["phone"] = $phone;
$context["fax"] = $fax;
$context["email"] = $email;
$context["contact_person"] = $contact_person;
$context["contact_phone"] = $contact_phone;
$context["website"] = $website;
$context["hours"] = $hours;
$context["img_loc"] = $img_loc;
$context["description"] = $description;
$context["lat"] = $lat;
$context["long"] = $long;

$context["cats"] = $cats;
$context["subcats"] = makeOptions($subcatsarr, $subcategory);
$context["json"] = $json_buff;

Db::CloseConnection($conn);

echo $twig->render('location.html', $context);