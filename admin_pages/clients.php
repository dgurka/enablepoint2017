<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$menus = Db::ExecuteQuery("SELECT * FROM customers WHERE locked = 0 ORDER BY username ASC", $conn);

$custmenu = "<h3>Client Accounts</h3>";

$custmenu .= "<ul>";

foreach ($menus as $value) 
{
	
	$menukey = $value["ID"];
	
	$custmenu .= "<li>" . $value["username"] . " (email: ".$value['email'].") <button class='btn' onclick='editUser(".$value['ID'].")'>edit</button>"; 
	
	//$custmenu .= " <button class='btn' onclick='deleteUser(".$value['ID'].")'>Delete</button>";

}

$custmenu .= "</ul>";


$lockedcustomers = Db::ExecuteQuery("SELECT * FROM customers WHERE locked = 1 ORDER BY username ASC", $conn);

if(count($lockedcustomers))
{
	$custmenu .= "<h3>Locked User Accounts</h3>";
	$custmenu .= "<ul>";

	foreach ($lockedcustomers as $lu) 
	{
		$custmenu .= "<li>" . $lu["username"] . " (email: ".$lu['email'].")";
		$luid = $lu["ID"];

		$custmenu .= " <button class='btn' onclick='editUser($luid)'>edit</button>";


		//$custmenu .= " <button class='btn' onclick='deleteVideo($luid)'>Delete</button>";
		
		$custmenu .= "</li>";
	}

	$custmenu .= "</ul>";
}


$context["pagemenu"] = $custmenu;
$context["HAS_MAIN_PAGE"] = MAIN_PAGE == "";

echo $twig->render('customers.html', $context);