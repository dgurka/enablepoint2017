<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$title = Db::EscapeString(post("title"), $conn);
	$menukey = Db::EscapeString(post("menukey"), $conn);
	$linkoverride = Db::EscapeString(post("linkoverride"), $conn);
	$description = Db::EscapeString(post("description"), $conn);
	$content = Db::EscapeString(post("content"), $conn);

	if(isset($matches[1]))
	{
		$id = (int)$matches[1];

		$query = "UPDATE custpage SET title = '$title', menukey = '$menukey', linkoverride = '$linkoverride', description = '$description', content = '$content' WHERE ID = $id";

		Db::ExecuteNonQuery($query, $conn);
		redirect(URL_ROOT . "admin/custpage/" . $id . "/");
	}
	else
	{
		$query = "INSERT INTO custpage (title, menukey, linkoverride, description, content) VALUES ('$title', '$menukey', '$linkoverride', '$description', '$content')";

		Db::ExecuteNonQuery($query, $conn);
		$id = Db::GetLastInsertID($conn);
		redirect(URL_ROOT . "admin/custpage/" . $id . "/");
	}

	exit();
}

if(isset($matches[1]))
{
	$id = (int)$matches[1];
	$context["id"] = $id;
	$custpage = Db::ExecuteFirst("SELECT * FROM custpage WHERE ID = $id", $conn);
	$title = str_replace("\"", "&quot;", $custpage["title"]);
	$menukey = $custpage["menukey"];
	$linkoverride = $custpage["linkoverride"];
	$description = $custpage["description"];
	$content = $custpage["content"];
}
else
{
	$title = "";
	$menukey = -1;
	$linkoverride = "";
	$description = "";
	$content = "";
}

$menuselect = array();

if($menukey == "-1"){
	$menuselect[] = "<option value='-1' selected='selected'>Not Assigned to a Client</option>";
	$menuselect[] = "<option value='999'>Available to All Clients</option>";
} else if($menukey == "999"){
	$menuselect[] = "<option value='-1'>Not Assigned to a Client</option>";
	$menuselect[] = "<option value='999' selected='selected'>Available to All Clients</option>";
} else {
	$menuselect[] = "<option value='-1'>Not Assigned to a Client</option>";
	$menuselect[] = "<option value='999'>Available to All Clients</option>";
}


//$menuitems = Db::ExecuteQuery("SELECT * FROM menu ORDER BY ID", $conn);
$menuitems = Db::ExecuteQuery("SELECT * FROM customers ORDER BY ID", $conn);

/*
This method works but generates an interested result:
*all* custpages are listed then *all* have their subcustpages listed, 
so "Videos" shows up under "About", and then again at the bottom as its own entry, with all its subcustpages

As stated above this function allows the user to see every custpage, but the nesting may be a bit confusing

Another option may be to simply list all the custpages without any nesting? 

*/

foreach ($menuitems as $value) 
{
	$mid = $value["ID"];
	$mname = $value["username"];
	$m = "<option value=\"$mid\" title=\"$mid\"";
	if($mid == $menukey){
		$m .= " selected='selected'";
	}
	$m .= ">&bull; $mname</option>";


	$menuselect[] = $m;
	
	
	/*// This code generates a list of custpages under a certain menu key
	$submenuitems = Db::ExecuteQuery("SELECT * FROM customers WHERE ID = ".$mid."", $conn);
	foreach ($submenuitems as $value) 
	{
		$pid = $value["ID"];
		$pname = $value["username"];
		$p = "<option value=\"".$pid."\" title=\"$pid\"";
		if($pid == $menukey){
			$p .= " selected='selected'";
		}
		$p .= ">&mdash; $pname</option>";
	
		$menuselect[] = $p;
	*/	
		/*$subsubmenuitems = Db::ExecuteQuery("SELECT ID, title FROM custpage WHERE menukey = ".$pid." ORDER BY menuorder DESC, ID", $conn);
		foreach ($subsubmenuitems as $svalue) 
		{
			$spid = $svalue["ID"];
			$spname = $svalue["title"];
			$sp = "<option value=\"".$spid."\" title=\"$spid\"";
			if($spid == $menukey){
				$sp .= " selected='selected'";
			}
			$sp .= ">&mdash;&mdash; $spname</option>";
		
			$menuselect[] = $sp;
		}
	}*/

}

$context["title"] = $title;
$context["linkoverride"] = $linkoverride;
$context["description"] = $description;
$context["content"] = $content;
$context["menuselect"] = implode("", $menuselect);

echo $twig->render('custpage.html', $context);