<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$dispmsg = "";

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$username = Db::EscapeString(post("username"), $conn);
	$locked = Db::EscapeString(post("locked"), $conn);
	$email = Db::EscapeString(post("email"), $conn);
	$notes = Db::EscapeString(post("notes"), $conn);
	$password = Db::EscapeString(post("password"), $conn);
	$md5pw = md5($password);
	$dispmsg = "Your changes have been saved.";
	$heading = "Edit";

	
	if(isset($matches[1]))
	{
		$id = (int)$matches[1];

		if(isset($password))
		{
			$query = "UPDATE customers SET username = '$username', email = '$email', password = '$md5pw', notes = '$notes', locked = '$locked' WHERE ID = $id";
			Db::ExecuteNonQuery($query, $conn);
		} else { 
			$query = "UPDATE customers SET username = '$username', email = '$email', notes = '$notes', locked = '$locked' WHERE ID = $id";
			Db::ExecuteNonQuery($query, $conn);	
		}
	}
	else
	{
		$query = "INSERT INTO customers (username, password, email, notes, locked) VALUES (
'$username', '$md5pw', '$email', '$notes', '$locked')";

		Db::ExecuteNonQuery($query, $conn);
		$id = Db::GetLastInsertID($conn);
		redirect(URL_ROOT . "admin/client/" . $id . "/");
	}

	redirect(URL_ROOT . "admin/clients/");
	//exit();
}

if(isset($matches[1]))
{
	$id = (int)$matches[1];
	$context["id"] = $id;
	$vid = Db::ExecuteFirst("SELECT * FROM customers WHERE ID = $id", $conn);
	$username = $vid["username"];
	$password = $vid["password"];
	$email = $vid["email"];
	$notes = $vid["notes"];
	$locked = $vid["locked"];
	$heading = "Edit";
	$dispmsg = "";
}
else
{
	$username = "";
	$password = "";
	$email = "";
	$notes = "";
	$locked = "";

	$heading = "Add";
	$dispmsg = "";
}

$context["username"] = $username;
$context["password"] = "";
$context["email"] = $email;
$context["notes"] = $notes;
$context["locked"] = "";


$context["heading"] = $heading;
$context["dispmsg"] = $dispmsg;

if($locked == "0"){
	$context["enabled"] = "checked";
} else {
	$context["disabled"] = "checked";
}


echo $twig->render('customer.html', $context);