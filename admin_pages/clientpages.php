<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$menus = Db::ExecuteQuery("SELECT * FROM customers ORDER BY ID", $conn);

$pagemenu = "";

foreach ($menus as $value) 
{

	$menukey = $value["ID"];
	$pages = Db::ExecuteQuery("SELECT * FROM custpage WHERE menukey = $menukey ORDER BY ID", $conn);
	if(count($pages))
	{
		$pagemenu .= "<strong>" . $value["username"] . "</strong>";

		$pagemenu .="<ul>";
		foreach ($pages as $i => $page) 
		{
			$pagemenu .= "<li>" . $page["title"];
			$pageid = $page["ID"];

			$pagemenu .= "&nbsp (ID: " . $pageid  . ") <button class='btn' onclick='editPage($pageid)'>edit</button></li>";

			$pagemenu .= "</li>";
		}
			
		$pagemenu .= "</ul>";
	} else {
				$pagemenu .= "<strong>" . $value["username"] . "</strong>";
				$pagemenu .= "<ul><li>No Sub Pages to Show</li></ul>";
	}
}


$pages = Db::ExecuteQuery("SELECT ID, title FROM custpage WHERE menukey = 999 ORDER BY ID", $conn);

if(count($pages))
{
	$pagemenu .= "<h2>Helpdesk Pages</h2><ul>";
	foreach ($pages as $i => $page) 
	{
		$pagemenu .= "<li>" . $page["title"];
		$pageid = $page["ID"];

		$pagemenu .= "&nbsp (ID: " . $pageid  . ") <button class='btn' onclick='editPage($pageid)'>edit</button>";

		$pagemenu .= "</li>";
	}

	$pagemenu .= "</ul>";
}

$pages = Db::ExecuteQuery("SELECT ID, title FROM custpage WHERE menukey = -1 ORDER BY ID", $conn);

if(count($pages))
{
	$pagemenu .= "<h2>Unassigned Pages</h2><ul>";
	foreach ($pages as $i => $page) 
	{
		$pagemenu .= "<li>" . $page["title"];
		$pageid = $page["ID"];

		$pagemenu .= "&nbsp (ID: " . $pageid  . ") <button class='btn' onclick='editPage($pageid)'>edit</button>";

		$pagemenu .= "</li>";
	}

	$pagemenu .= "</ul>";
}


$context["pagemenu"] = $pagemenu;
$context["HAS_MAIN_PAGE"] = MAIN_PAGE == "";

echo $twig->render('custpages.html', $context);